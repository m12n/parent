cask '\${project.artifactId}' do
  version '\${project.version}'
  sha256 '%SHA_256%'

  url "https://github.com/m12n/\${project.artifactId}/releases/download/\${project.version}/\${project.artifactId}-\${project.version}.dmg"
  name '\${project.name}'
  homepage 'https://github.com/m12n/\${project.artifactId}'

  app '\${project.name}.app'

  caveats do
    depends_on_java('8')
  end

  zap delete: [
    '~/Library/Application Support/\${project.name}',
    '~/Library/Logs/\${project.name}',
    '~/Library/LaunchAgents/\${project.groupId}.\${project.artifactId}.plist'
  ]
end
