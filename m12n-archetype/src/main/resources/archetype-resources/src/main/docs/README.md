# ${project.name}

[![Build Status](https://travis-ci.org/m12n/${project.name}.svg?branch=master)](https://travis-ci.org/m12n/${project.name}) [![Coverage Status](https://coveralls.io/repos/github/m12n/${project.name}/badge.svg?branch=master)](https://coveralls.io/github/m12n/${project.name}?branch=master)


## Installation

Add the following dependency:

```
<dependency>
    <groupId>${project.groupId}</groupId>
    <artifactId>${project.artifactId}</artifactId>
    <version>${project.version}</version>
</dependency>
```

Add the following repository:

```
<repository>
    <id>m12n-maven-repo-releases</id>
    <url>http://maven.m12n.org/releases</url>
</repository>
```
