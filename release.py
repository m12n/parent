#!/usr/bin/env python
import hashlib
import json
import os
import re
import shlex
import sys
from subprocess import call
from xml.etree import ElementTree

import requests


def input(text, default=None):
    defaultText = '' if default == None else ' (%s)' % (default)
    msg = '%s%s: ' % (text, defaultText)
    value = raw_input(msg)
    if value is '':
        if default == None:
            exit(-1)
        else:
            value = default
    return value


def execute(args, cmd):
    args += shlex.split(cmd)
    print(args)
    if not debug_mode:
        call(args)


def mvn(cmd):
    execute(['mvn', '-q'], cmd)


def git(cmd):
    execute(['git'], cmd)


def create_homebrew_file(artifact):
    checksum = hashlib.sha256(
        open(artifact.path, 'rb').read()).hexdigest()
    fileName = '%s.rb' % (artifact.id)
    with open(fileName, 'r') as file:
        data = file.read()
    data = data.replace('%SHA_256%', checksum)
    with open(fileName, 'w') as file:
        file.write(data)


def create_release(token, artifact):
    headers = {'Authorization': 'token %s' % (token),
               'User-Agent': artifact.id,
               'Accept': 'application/vnd.github.v3+json'}
    url = 'https://api.github.com/repos/m12n/%s/releases' % (artifact.id)
    data = '{"tag_name":"%s"}' % (artifact.version)
    print url, headers, data
    r = requests.post(url,
                      headers=headers,
                      data=data)
    release_id = json.loads(r.text)['id']
    url = 'https://uploads.github.com/repos/m12n/%s/releases/%s/assets?name=%s' % \
          (artifact.id, release_id, artifact.name)
    headers['Content-Type'] = 'application/octet-stream'
    data = open(artifact.name, 'rb').read()
    print url, headers
    r = requests.post(url,
                      headers=headers,
                      data=data)


class Artifact:
    def __init__(self, id, name, version):
        self.id = id
        self.version = version
        self.name = '%s-%s.dmg' % (id, version)
        self.path = 'target/%s' % (self.name)

    def exists(self):
        return os.path.isfile(self.path)


class SemanticVersion:
    p = re.compile(r'(\d*)\.(\d*)(\.(\d*))(.*)')

    def __init__(self, version_str):
        self.version_str = version_str
        m = SemanticVersion.p.match(version_str)
        if (m):
            self.major = int(m.group(1))
            self.minor = int(m.group(2))
            self.patch = int(m.group(4))
            self.suffix = m.group(5)

    def to_string(self, major, minor, patch):
        return '%s.%s.%s' % (major, minor, patch)

    def __str__(self):
        return self.version_str

    def next(self):
        return self.to_string(self.major, self.minor, self.patch + 1)

    def release(self):
        return self.to_string(self.major, self.minor, self.patch)


if __name__ == '__main__':
    # Parse Maven POM
    prefix = '{http://maven.apache.org/POM/4.0.0}'
    pom = ElementTree.parse('pom.xml')
    project_version = SemanticVersion(pom.findtext('%sversion' % (prefix)))
    project_name = pom.findtext('%sname' % (prefix))
    project_artifact_id = pom.findtext('%sartifactId' % (prefix))

    # Environment variables
    github_oauth_token = os.environ['GITHUB_OAUTH_TOKEN']

    # Command line arguments
    debug_mode = False
    if len(sys.argv) > 1:
        arg1 = sys.argv[1]
        debug_mode = arg1 is 'debug'
        if (arg1 is 'revert'):
            git('tag -d %s' % (project_version))
            git('push origin :%s' % (project_version))
            git('push github :%s' % (project_version))
            git('reset --hard HEAD~1')

    # User input
    release_version = input('Release version', project_version.release())
    next_version = '%s-SNAPSHOT' % (
        input('Next version, SNAPSHOT will be added automatically', project_version.next()))

    # Variables
    release_artifact = Artifact(project_artifact_id, project_name, release_version)

    mvn('versions:set -DnewVersion=%s' % (release_version))
    goal = 'package' if release_artifact.exists() else 'deploy'
    mvn('clean %s' % (goal))
    mvn('versions:commit')

    if release_artifact.exists():
        create_homebrew_file(release_artifact)

    git('add --all')
    git('commit -m "Created release version %s"' % (release_version))
    git('tag %s' % (release_version))

    git('push --tags')
    git('push --tags github')

    if release_artifact.exists():
        create_release(github_oauth_token, release_artifact)

    git('checkout -B dev')
    git('rebase master')

    mvn('versions:set -DnewVersion=%s' % (next_version))
    mvn('versions:commit')
    mvn('clean verify')

    git('add --all')
    git('commit -m "Starting to work on %s"' % (next_version))

    git('push --all')
    git('push --all github')
