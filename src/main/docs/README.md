# Parent POM

This is the parent POM of the m12n projects.

## Installation

Add the following parent to your pom.xml:

```
<parent>
    <groupId>${project.groupId}</groupId>
    <artifactId>${project.artifactId}</artifactId>
    <version>${project.version}</version>
</parent>
```

Additionally, you should add the following repository, because m12n
artifacts are not hosted on Maven Central:

```
<repository>
    <id>m12n-maven-repo-releases</id>
    <url>http://maven.m12n.org/releases</url>
</repository>
```

## Features

- Distribution management: The parent POM is already prepared to support
  m12n compliant distribution management to m12n repositories.
- Java compiler settings (default: Java 1.8)
- Source artifacts will be deployed
- Code coverage support including Coveralls integration. Note, that you
  need to pass the `coveralls.repo.token` as environment variable to
  your build, never add this token to source control
- Basic documentation with filtering (put a `README.md` into the
  `src/main/docs` folder, or override property `docs.src.dir`
- JUnit integration
- Properties support: in your project you can simply override the 
  versions of all dependencies and plugins
  

---

# m12n Maven Archetype

Creates new projects with the m12n parent POM.

## Usage

Run the following in your command line:

```
mvn archetype:generate \
-DarchetypeGroupId=org.m12n \
-DarchetypeArtifactId=m12n-archetype \
-DarchetypeVersion=${project.version} \
-DarchetypeRepository=http://maven.m12n.org/releases \
-DgroupId=org.m12n \
-DartifactId=<your-artifact-id>
```
